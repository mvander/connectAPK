#!/bin/sh

#  setup.sh
#  
#
#  Created by Mark Van der Merwe on 6/10/16.
#

if [ $# -lt 1 ]; then
echo "Usage: <Test device ID, eg, pc599.emulab.net:8002>"
exit 1
fi

UENAME=$1

echo "Starting setup."

#Android SDK download -> Needed to run monkeyrunner script to open phone.
wget https://dl.google.com/android/android-sdk_r24.4.1-linux.tgz
tar zxvf android-sdk_r24.4.1-linux.tgz
echo "Android SDK downloaded."

#ADB install -> Needed to access UEs remotely.
sudo apt update
sudo apt-get install android-tools-adb
cd android-sdk-linux
mkdir platform-tools
cd /usr/bin
sudo cp adb ~/connectAPK/android-sdk-linux/platform-tools
export PATH=$PATH:~/connectAPK/android-sdk-linux/platform-tools:~/connectAPK/android-sdk-linux/tools
echo "ADB downloaded."

#Install JPG viewer -> Needed to view screenshots of the phone.
cd ~/connectAPK
sudo apt-get install gpicview
echo "GPicView installed."

#Java install -> Needed to run monkeyrunner script to open phone.
sudo apt-get install default-jre
echo "Java downloaded."

