#!/bin/sh

#  screenshot.sh
#  
#
#  Created by Mark Van der Merwe on 6/13/16.
#

#Take screenshot.
adb shell screencap /sdcard/screenstart.png
adb pull /sdcard/screenstart.png /users/$USER/connectAPK/screenstart.png
gpicview screenstart.png
echo "Screenshot complete."