#!/bin/sh

#  runtest.sh
#  
#
#  Created by Mark Van der Merwe on 6/9/16.
#

DEVICE=$1

PKG_NAME="com.example.android.theconnectseries.test"
INSTRUMENTATION="com.example.android.theconnectseries.test/android.test.InstrumentationTestRunner"

echo "Wake up device $DEVICE ..."
monkeyrunner wakeup-monkey.py $DEVICE
if [ $? != 0 ]; then
echo "$wakeup device FAILED!"
exit 1
fi

echo "Run instrumentation $INSTRUMENTATION ..."
adb shell am instrument $INSTRUMENTATION

echo "Collecting logfile."
#adb shell rm /sdcard/log.txt
#echo "Previous logfile removed"
adb logcat | grep --line-buffered "myApp"

echo "Done."
