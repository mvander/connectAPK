#!/bin/sh

#  openapp.sh
#  
#
#  Created by Mark Van der Merwe on 6/13/16.
#

if [ $# -lt 1 ]; then
echo "Usage: <Test device ID, eg, pc599.emulab.net:8002>"
exit 1
fi

UENAME=$1

#Waking phone/taking screenshot.
bash wakeup.sh $UENAME
echo "Phone awake."

#Install example app.
#adb install app-debug.apk

#Open android clock app.
adb shell am start com.android.deskclock

#Take screenshot.
bash screenshot.sh $UENAME

echo "Done."