#!/bin/sh

#  wakeup.sh
#  
#
#  Created by Mark Van der Merwe on 6/13/16.
#

if [ $# -lt 1 ]; then
echo "Usage: <Test device ID, eg, pc599.emulab.net:8002>"
exit 1
fi

UENAME=$1

echo "Waking device."

#Uses wakeup-monkey.py file to wake up phone.
monkeyrunner wakeup-monkey.py $UENAME
echo "Phone up."

#Take screenshot.
bash screenshot.sh $UENAME